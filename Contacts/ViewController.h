//
//  ViewController.h
//  Contacts
//
//  Created by Aung on 20/6/15.
//  Copyright (c) 2015 Aung Phyoe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DBManager.h"
#import "ContactCell.h"
#import "MyContactsManager.h"

@interface ViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,MyContactsManagerDelegate>{



}
@property (nonatomic,retain) NSArray *contactList;
@property (nonatomic,retain) NSArray *newcontactList;
@property (nonatomic,retain) NSMutableArray *differentContactList;
@property (weak, nonatomic) IBOutlet UITableView *dataTableView;
@property (weak, nonatomic) IBOutlet UILabel *lblStatus;
@property (weak, nonatomic) IBOutlet UIButton *btnCheck;
- (IBAction)checkContactAction:(UIButton *)sender;

@end

