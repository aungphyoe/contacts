//
//  ContactCell.h
//  Contacts
//
//  Created by Aung on 20/6/15.
//  Copyright (c) 2015 Aung Phyoe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblNumber;

@end
