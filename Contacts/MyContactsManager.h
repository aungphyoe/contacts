//
//  MyContactsManager.h
//  Contacts
//
//  Created by Aung on 20/6/15.
//  Copyright (c) 2015 Aung Phyoe. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AddressBook/AddressBook.h>
#import <UIKit/UIKit.h>
#import "MContact.h"
@protocol MyContactsManagerDelegate;

@interface MyContactsManager : NSObject
@property (strong, nonatomic) NSObject<MyContactsManagerDelegate> *delegate;
+ (id)sharedManager;
- (void)importContacts:(void (^)(NSArray *contacts))contactsHandler;
- (void)checkAddressBookUpdate;
@end


@protocol MyContactsManagerDelegate <NSObject>

-(void)addressBookContactDidChange:(NSArray*)newlist;

@end