//
//  MContact.h
//  Contacts
//
//  Created by Aung on 20/6/15.
//  Copyright (c) 2015 Aung Phyoe. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MContact : NSObject
@property (nonatomic,assign) NSInteger contactID;
@property (nonatomic,retain) NSString *firstName;
@property (nonatomic,retain) NSString *lastName;
@property (nonatomic,retain) NSString *middleName;
@property (nonatomic,retain) NSArray *phoneList;
-(NSString*)getFullContact;
@end
