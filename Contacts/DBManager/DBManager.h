//
//  DBManager.h
//  Contacts
//
//  Created by Aung on 20/6/15.
//  Copyright (c) 2015 Aung Phyoe. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AddressBook/AddressBook.h>
#import <sqlite3.h>
#import "MContact.h"
@interface DBManager : NSObject  {
    sqlite3 *_database;
}

+ (DBManager*)database;
-(void)updateAddressbook:(ABAddressBookRef)addressBookRef;
-(NSMutableArray*)getStoredContacts;
-(BOOL)insertPerson:(MContact*)person;
-(void)deleteAddressbook;
@end
