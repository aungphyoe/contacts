//
//  DBManager.m
//  Contacts
//
//  Created by Aung on 20/6/15.
//  Copyright (c) 2015 Aung Phyoe. All rights reserved.
//

#import "DBManager.h"

#import "MContact.h"

@interface DBManager()

@property (nonatomic, strong) NSString *documentsDirectory;

@property (nonatomic, strong) NSString *databaseFilename;

@property (nonatomic, strong) NSMutableArray *arrResults;

@end

@implementation DBManager
static DBManager *_databasemanager;

+ (DBManager*)database {
    if (_databasemanager == nil) {
        _databasemanager = [[DBManager alloc] initWithDatabaseFilename:@"database.sqlite3"];
    }
    return _databasemanager;
}

#pragma mark - Initialization

-(instancetype)initWithDatabaseFilename:(NSString *)dbFilename{
    self = [super init];
    if (self) {
        // Set the documents directory path to the documentsDirectory property.
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        self.documentsDirectory = [paths objectAtIndex:0];
        
        // Keep the database filename.
        self.databaseFilename = dbFilename;
        
        // Copy the database file into the documents directory if necessary.
        [self copyDatabaseIntoDocumentsDirectory];
    }
    return self;
}

#pragma mark - Private method implementation

-(void)copyDatabaseIntoDocumentsDirectory{
    // Check if the database file exists in the documents directory.
    NSString *destinationPath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    if (![[NSFileManager defaultManager] fileExistsAtPath:destinationPath]) {
        // The database file does not exist in the documents directory, so copy it from the main bundle now.
        NSString *sourcePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:self.databaseFilename];
        NSError *error;
        [[NSFileManager defaultManager] copyItemAtPath:sourcePath toPath:destinationPath error:&error];
        
        // Check if any error occurred during copying and display it.
        if (error != nil) {
            NSLog(@"%@", [error localizedDescription]);
        }
    }
}


- (void)dealloc {
    sqlite3_close(_database);
    
}


-(NSMutableArray*)getStoredContacts{

    sqlite3 *sqlite3Database;
    
    // Set the database file path.
    NSString *databasePath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
  
    NSString *query = @"SELECT contactID,firstname, lastname, middlename, phones FROM contact ORDER BY firstname";
    NSMutableArray *tempList=[[NSMutableArray alloc]init];
    BOOL openDatabaseResult = sqlite3_open([databasePath UTF8String], &sqlite3Database);
    if(openDatabaseResult == SQLITE_OK) {
        // Declare a sqlite3_stmt object in which will be stored the query after having been compiled into a SQLite statement.
        sqlite3_stmt *compiledStatement;
      
        // Load all data from database to memory.
        BOOL prepareStatementResult = sqlite3_prepare_v2(sqlite3Database, [query UTF8String], -1, &compiledStatement, NULL);
        if(prepareStatementResult == SQLITE_OK) {
            
            while (sqlite3_step(compiledStatement) == SQLITE_ROW) {
                MContact *person=[[MContact alloc]init];
                
                    person.contactID=(NSInteger) sqlite3_column_int(compiledStatement, 0);
                    person.firstName = [[NSString alloc] initWithUTF8String:(char *) sqlite3_column_text(compiledStatement, 1)];
                    person.lastName = [[NSString alloc] initWithUTF8String:(char *) sqlite3_column_text(compiledStatement, 2)];
                    person.middleName = [[NSString alloc] initWithUTF8String:(char *) sqlite3_column_text(compiledStatement, 3)];
                
                NSString * combinedNumber = [[NSString alloc] initWithUTF8String:(char *) sqlite3_column_text(compiledStatement, 4)];
                
                person.phoneList=[combinedNumber componentsSeparatedByString:@";"];
                [tempList addObject:person];
                
                
            }
            sqlite3_finalize(compiledStatement);
        }
       
    }
    return tempList;
}

-(BOOL)insertPerson:(MContact*)person{
    
    if ([self isExistingContact:person]) {
        
        if ([self isPersonContactIsChanged:person]) {
            
            
            [self updatePerson:person];
        }
        
        return YES;
        
    }
    
    
    
    sqlite3 *sqlite3Database;
    BOOL success=NO;
    // Set the database file path.
    NSString *databasePath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    
    
    NSString * combinedNumber = [person.phoneList componentsJoinedByString:@";"];

    NSLog(@"combinedStuff %@",combinedNumber);
    
    NSString *insertSQL = [NSString stringWithFormat:
                           @"INSERT INTO contact (contactID,firstname, lastname, middlename, phones) VALUES (\"%li\",\"%@\",\"%@\",\"%@\",\"%@\")",
                           person.contactID,person.firstName,person.lastName,person.middleName,combinedNumber];
    
   BOOL openDatabaseResult = sqlite3_open([databasePath UTF8String], &sqlite3Database);
    if(openDatabaseResult == SQLITE_OK) {
        // Declare a sqlite3_stmt object in which will be stored the query after having been compiled into a SQLite statement.
        sqlite3_stmt *compiledStatement;
        
        // Load all data from database to memory.
        BOOL prepareStatementResult = sqlite3_prepare_v2(sqlite3Database, [insertSQL UTF8String], -1, &compiledStatement, NULL);
        if(prepareStatementResult == SQLITE_OK) {
            
            if (sqlite3_step(compiledStatement) == SQLITE_DONE) {
                
                success=YES;
                
            }
            
            
            
            sqlite3_finalize(compiledStatement);
        }
        // Close the database.
        sqlite3_close(sqlite3Database);
    }



    return success;
}


-(BOOL)updatePerson:(MContact*)person{
    sqlite3 *sqlite3Database;
    BOOL success=NO;
    // Set the database file path.
     NSString * combinedNumber = [person.phoneList componentsJoinedByString:@";"];
    
    NSString *databasePath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    
    NSString *insertSQL = [NSString stringWithFormat:
                           @"UPDATE contact SET firstname = \"%@\", lastname= \"%@\", middlename =\"%@\",phones =\"%@\" WHERE contactID = \"%li\"",
                           person.firstName,person.lastName,person.middleName,combinedNumber,person.contactID];
    
    BOOL openDatabaseResult = sqlite3_open([databasePath UTF8String], &sqlite3Database);
    if(openDatabaseResult == SQLITE_OK) {
        // Declare a sqlite3_stmt object in which will be stored the query after having been compiled into a SQLite statement.
        sqlite3_stmt *compiledStatement;
        
        // Load all data from database to memory.
        BOOL prepareStatementResult = sqlite3_prepare_v2(sqlite3Database, [insertSQL UTF8String], -1, &compiledStatement, NULL);
        if(prepareStatementResult == SQLITE_OK) {
            
            if (sqlite3_step(compiledStatement) == SQLITE_DONE) {
                
                success=YES;
                
            }
            
            
            
            sqlite3_finalize(compiledStatement);
        }
        // Close the database.
        sqlite3_close(sqlite3Database);
    }
    
    
    
    return success;
}

-(BOOL)isExistingContact:(MContact*)person{

    sqlite3 *sqlite3Database;
    BOOL isExisting=NO;
    // Set the database file path.
    NSString *databasePath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    
    NSString *query = [NSString stringWithFormat:@"SELECT contactID FROM contact WHERE contactID =\"%li\"",person.contactID];
    
    BOOL openDatabaseResult = sqlite3_open([databasePath UTF8String], &sqlite3Database);
    if(openDatabaseResult == SQLITE_OK) {
        // Declare a sqlite3_stmt object in which will be stored the query after having been compiled into a SQLite statement.
        sqlite3_stmt *compiledStatement;
        
        // Load all data from database to memory.
        BOOL prepareStatementResult = sqlite3_prepare_v2(sqlite3Database, [query UTF8String], -1, &compiledStatement, NULL);
        if(prepareStatementResult == SQLITE_OK) {
            
            while (sqlite3_step(compiledStatement) == SQLITE_ROW) {
                isExisting=YES;
                
                
            }
            sqlite3_finalize(compiledStatement);
        }
        // Close the database.
        sqlite3_close(sqlite3Database);
    }
    
    
    return isExisting;
    


}





-(BOOL)isPersonContactIsChanged:(MContact*)person{
    sqlite3 *sqlite3Database;
    MContact *tempPerson=nil;
    // Set the database file path.
    NSString *databasePath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    
    NSString *query = [NSString stringWithFormat:@"SELECT contactID,firstname, lastname, middlename, phones FROM contact WHERE contactID =\"%li\"",person.contactID];

    BOOL openDatabaseResult = sqlite3_open([databasePath UTF8String], &sqlite3Database);
    if(openDatabaseResult == SQLITE_OK) {
        // Declare a sqlite3_stmt object in which will be stored the query after having been compiled into a SQLite statement.
        sqlite3_stmt *compiledStatement;
        
        // Load all data from database to memory.
        BOOL prepareStatementResult = sqlite3_prepare_v2(sqlite3Database, [query UTF8String], -1, &compiledStatement, NULL);
        if(prepareStatementResult == SQLITE_OK) {
            
            while (sqlite3_step(compiledStatement) == SQLITE_ROW) {
                tempPerson=[[MContact alloc]init];
                
                tempPerson.contactID=(NSInteger) sqlite3_column_int(compiledStatement, 0);
                tempPerson.firstName = [[NSString alloc] initWithUTF8String:(char *) sqlite3_column_text(compiledStatement, 1)];
                tempPerson.lastName = [[NSString alloc] initWithUTF8String:(char *) sqlite3_column_text(compiledStatement, 2)];
                tempPerson.middleName = [[NSString alloc] initWithUTF8String:(char *) sqlite3_column_text(compiledStatement, 3)];
                 NSString * combinedNumber = [[NSString alloc] initWithUTF8String:(char *) sqlite3_column_text(compiledStatement, 4)];
                
                tempPerson.phoneList=[combinedNumber componentsSeparatedByString:@";"];
                
                
            }
            sqlite3_finalize(compiledStatement);
        }
        // Close the database.
        sqlite3_close(sqlite3Database);
    }
    
    if (tempPerson) {
      
        NSString *old=[person getFullContact];
        NSString *newe=[tempPerson getFullContact];
        
        if ([old isEqualToString:newe]) {
            
        }else{
        
            return YES;
        
        }
        
    }else{
    
    
        return YES;
    }
    
    
    return NO;

}



-(void)deleteAddressbook{
    
    sqlite3 *sqlite3Database;
    
    // Set the database file path.
    NSString *databasePath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    NSString *query = @"delete FROM contact";
    BOOL openDatabaseResult = sqlite3_open([databasePath UTF8String], &sqlite3Database);
    if(openDatabaseResult == SQLITE_OK) {
        // Declare a sqlite3_stmt object in which will be stored the query after having been compiled into a SQLite statement.
        sqlite3_stmt *compiledStatement;
        
        // Load all data from database to memory.
        BOOL prepareStatementResult = sqlite3_prepare_v2(sqlite3Database, [query UTF8String], -1, &compiledStatement, NULL);
        if(prepareStatementResult == SQLITE_OK) {
            
            while (sqlite3_step(compiledStatement) == SQLITE_ROW) {
               
                
            }
            
            
            
            sqlite3_finalize(compiledStatement);
        }
    }
    

}

@end
