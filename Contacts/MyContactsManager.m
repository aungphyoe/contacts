//
//  MyContactsManager.m
//  Contacts
//
//  Created by Aung on 20/6/15.
//  Copyright (c) 2015 Aung Phyoe. All rights reserved.
//

#import "MyContactsManager.h"
#import "DBManager.h"

@interface MyContactsManager ()

@property (assign, nonatomic) ABAddressBookRef addressBook;

@end
@implementation MyContactsManager

+ (id)sharedManager{

    static MyContactsManager *contactManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        contactManager = [[self alloc] init];
    });
    return contactManager;
}


- (id)init
{
    self = [super init];
    
    if(self)
    {
        CFErrorRef *error = NULL;
        self.addressBook = ABAddressBookCreateWithOptions(NULL, error);
        [self checkAddressBookUpdate];
    }
    
    return self;
}


- (void)importContacts:(void (^)(NSArray *))contactsHandler
{
    if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusDenied || ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusRestricted)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Address Book Access Denied" message:@"Please grant us access to your Address Book in Settings -> Privacy -> Contacts" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:@"OK", nil];
        [alert show];
        return;
    }
    
    if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusNotDetermined)
    {
        CFErrorRef *error = nil;
        ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(nil, error);
        ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
            if (granted)
            {
                
               
                NSMutableArray *contactsList = [(__bridge NSArray *)ABAddressBookCopyArrayOfAllPeople(addressBook) mutableCopy];
                contactsHandler([[NSArray alloc] initWithArray:[self extractContacts:contactsList]]);
            }
        });
        return;
    }
    
    if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized)
    {
        CFErrorRef *error = nil;
        ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(nil, error);
        NSMutableArray *contactsList = [(__bridge NSArray *)ABAddressBookCopyArrayOfAllPeople(addressBook) mutableCopy];
        contactsHandler([[NSArray alloc] initWithArray:[self extractContacts:contactsList]]);
        return;
    }
}

- (NSMutableArray *)extractContacts:(NSMutableArray *)contactsList
{
    NSMutableArray *importedContacts = [[NSMutableArray alloc] init];
    
    [contactsList enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        ABRecordRef record = (__bridge ABRecordRef)obj;
        MContact *person = [[MContact alloc] init];
        
        // Contact ID
        ABRecordID contactID = ABRecordGetRecordID(record);
        person.contactID = contactID;
        
        // FirstName
        person.firstName = [self stringWithProperty:kABPersonFirstNameProperty fromContact:record];
        
        // middleName
        person.middleName = [self stringWithProperty:kABPersonMiddleNameProperty fromContact:record];
        
        // LastName
        person.lastName = [self stringWithProperty:kABPersonLastNameProperty fromContact:record];

        // Phone number(s)
        ABMultiValueRef phones = ABRecordCopyValue(record, kABPersonPhoneProperty);
        NSMutableArray *phonesArray = [[NSMutableArray alloc] init];
        for(CFIndex j = 0; j < ABMultiValueGetCount(phones); j++)
        {
            NSString *phoneNumber = (__bridge NSString *)(ABMultiValueCopyValueAtIndex(phones, j));
           
            [phonesArray addObject:phoneNumber];
        }
        person.phoneList =[[NSArray alloc]initWithArray: phonesArray];
        
        if ([person.firstName length]||[person.lastName length]||[person.middleName length]||[person.phoneList count]) {
          
            [importedContacts addObject:person];
        }
        
    }];
    
     [importedContacts sortUsingDescriptors:@[ [NSSortDescriptor sortDescriptorWithKey:@"firstName" ascending:YES] ]];
    return importedContacts;
}


-(NSString *)stringWithProperty:(ABPropertyID)property fromContact:(ABRecordRef)person
{
    CFTypeRef companyCFObject = ABRecordCopyValue(person, property);
    return (companyCFObject != nil) ? (__bridge NSString *)companyCFObject : @"";
}

- (void)checkAddressBookUpdate
{
    ABAddressBookRegisterExternalChangeCallback(self.addressBook, addressBookExternalChange, (__bridge void *)(self));
}


#pragma mark - delegate callback

void addressBookExternalChange(ABAddressBookRef __unused addressBookRef, CFDictionaryRef __unused info, void *context)
{
    MyContactsManager *manager = (__bridge MyContactsManager *)(context);
    if([manager.delegate respondsToSelector:@selector(addressBookContactDidChange:)])
    {
        
        
        [manager importContacts:^(NSArray *contacts)
         {
             
          [manager.delegate addressBookContactDidChange:contacts];
         }];

        
        
    }
    
}




@end
