//
//  MContact.m
//  Contacts
//
//  Created by Aung on 20/6/15.
//  Copyright (c) 2015 Aung Phyoe. All rights reserved.
//

#import "MContact.h"

@implementation MContact



-(NSString*)getFullContact{

    NSString * combinedNumber = [self.phoneList componentsJoinedByString:@";"];

    return[NSString stringWithFormat:@"%@ %@ %@ %@",self.firstName,self.middleName,self.lastName,combinedNumber];

    }

@end
