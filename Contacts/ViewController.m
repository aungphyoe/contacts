//
//  ViewController.m
//  Contacts
//
//  Created by Aung on 20/6/15.
//  Copyright (c) 2015 Aung Phyoe. All rights reserved.
//

#import "ViewController.h"



@interface ViewController ()

@end

@implementation ViewController



- (void)viewDidLoad {
    [super viewDidLoad];

 
    [[self.btnCheck layer] setCornerRadius:8.0f];
    [[self.btnCheck layer] setMasksToBounds:YES];
    [[self.btnCheck layer] setBorderWidth:1.0f];
    self.btnCheck.hidden=YES;
    
    self.lblStatus.text=@"Syncing is in wait";
  
    MyContactsManager *contactsManager = [MyContactsManager sharedManager];
    contactsManager.delegate=self;
    [contactsManager importContacts:^(NSArray *contacts)
     {
         
         for(MContact *person in contacts){
         
             [[DBManager database] insertPerson:person];
         
         }
         self.contactList=[[DBManager database] getStoredContacts];
        
          self.btnCheck.hidden=NO;
          self.lblStatus.text=@"Sync is done";
    
    }];

    // Do any additional setup after loading the view, typically from a nib.
}

#pragma mark - TableView Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{


    return 80;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.differentContactList count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ContactCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ContactCell"
                                                        forIndexPath:indexPath];
    
    MContact *person = [self.differentContactList objectAtIndex:indexPath.row];
    

    cell.lblName.text = [person.firstName stringByAppendingString:[NSString stringWithFormat:@" %@", person.lastName]];
    
  
    NSArray *phones = person.phoneList;
    
    if ([phones count] > 0) {
        NSString *phoneItem = phones[0];
        cell.lblNumber.text = phoneItem;
    }
    
    return cell;
}

-(void)addressBookContactDidChange:(NSArray*)newlist{
    self.newcontactList=nil;
    self.newcontactList=newlist;
   
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)checkContactAction:(UIButton *)sender {
    
    if ([self.newcontactList count]) {
        self.differentContactList=nil;
        
        if ([self.newcontactList count]<[self.contactList count]) {
            self.differentContactList=[self arraysContainSameObjects:self.contactList andOtherArray:self.newcontactList];
        }else{
            self.differentContactList=[self arraysContainSameObjects:self.newcontactList andOtherArray:self.contactList];
        }
        
        [self.dataTableView reloadData];
        
        self.lblStatus.text=[NSString stringWithFormat:@"%i Contact Changed",(int)[self.differentContactList count]];
        
        if ([self.differentContactList count]>2) {
            self.lblStatus.text=[NSString stringWithFormat:@"%i Contacts Changed",(int)[self.differentContactList count]];
        }
        
        
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            //Background Thread
             [[DBManager database] deleteAddressbook];
            for(MContact *person in self.newcontactList ){
                
                [[DBManager database] insertPerson:person];
                
            }
            
            
            self.contactList=nil;
            self.contactList=[[DBManager database] getStoredContacts];

            
            dispatch_async(dispatch_get_main_queue(), ^(void){
                //Run UI Updates
                
                
             
                
                self.newcontactList=nil;
            });
        });
        
     
        
    }else{
        self.differentContactList=nil;
        self.newcontactList=nil;
        self.lblStatus.text=@"No changes";
        [self.dataTableView reloadData];

    }
    
}

-(NSMutableArray*)arraysContainSameObjects:(NSArray *)array1 andOtherArray:(NSArray *)array2 {
    // quit if array count is different
    
    NSMutableArray *tempArray=[[NSMutableArray alloc]init];
   // MContact* tempperson;
    for (MContact* person in array1) {
        
        BOOL isSame=NO;
        NSString *firstContact=[person getFullContact];
        
        for(MContact *secondPerson in array2){
        
           NSString *secondContact=[secondPerson getFullContact];
            
            if ([firstContact isEqualToString:secondContact]) {
                
                isSame=YES;
                
                break;
                
            }
            
        
        }
        
        if (!isSame) {
            
            [tempArray addObject:person];
        }
    
    
    }
    
    return tempArray;
}


@end
